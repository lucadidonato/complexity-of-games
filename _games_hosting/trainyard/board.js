// Board model
function Board(board, trains, markers){
	//~ console.log(board);
	//~ console.log(trains);
	//~ console.log(markers);
	//~ console.log(k_i);
	//~ console.log(k);
    this.board = this.clone(board);
    this.state = [];
    this.trains = [];
    this.TIME = 0;
    this.markers = this.clone(markers);
    this.crashed = this.crashedAll = false;
    this.hittingTime = [];
    this.hittingDones = [];
    for (i = 0; i < trains.length; i++) this.trainPush(trains[i]);
    this.setup();
}

// Pls don't change or you'll mess gadgets!
Board.EMPTY = 0;
Board.BLOCK = 1;
Board.SPAWN = 2;
Board.COLOR = 3;
Board.ARRIV = 4;
Board.PAINT = 5;
Board.SCISS = 6;

// Train color constants + names
Board.T_C = {};
Board.T_C[Board.T_RED = 1] = "red";
Board.T_C[Board.T_BLUE = 2] = "blue";
Board.T_C[Board.T_YELLOW = 4] = "yellow";
Board.T_C[Board.T_PURPLE = Board.T_RED + Board.T_BLUE] = "purple";
Board.T_C[Board.T_BROWN = 100] = "brown";

// Gadget Highlight Color
Board.GH_GREENZONE = "#00CE00";
Board.GH_OTP = "#F8F8F8";
Board.GH_CANCROSS = "#0260F9";
Board.GH_CANNOTCROSS = "#FF3C00";
Board.GH_TERMINAL = "#F4F701";
Board.GH_AND = "#E79004";
Board.GH_REPLICATOR = "#C500C5";

// Some parameters
Board.BUFFER_HEIGHT = 6;

Board.prototype.restart = function(){
    this.stateLoad();
    this.setup();
}
Board.prototype.setup = function(){
    this.stateSave();
    this.onSetup();
    this.redraw();
    //~ this.update(true);
}
Board.prototype.trainPush = function(t){
    this.trains.push(t);
}
Board.prototype.cellSetup = function(r,c){
  cell = this.board[r][c];
  if (typeof(cell)=="string"){
    tmp = cell;
    if (tmp.length == 2)
        tmp = [tmp, tmp];
    else
        tmp = [tmp.substr(0,2), tmp.substr(2,2)];
    for (i = 0; i < 2; i++)
        if (tmp[i][0] > tmp[i][1])
            tmp[i] = tmp[i][1]+tmp[i][0];
  } else {
    cell.rail = "----";
    cell.entrance = "";
    switch (cell[0]){
      case Board.ARRIV:
        cell.rail = "DUDU";
        cell.entrance = "D";
        if (cell.length > 2){
            if (cell[2] == 90 || cell[2] == 270) cell.rail = "LRLR";
            if (cell[2] == 90) cell.entrance = "L";
            if (cell[2] == 180) cell.entrance = "U";
            if (cell[2] == 270) cell.entrance = "R";
        }
        break;
      case Board.PAINT:
        cell.rail = "DUDU";
        cell.entrance = "DU";
        if (cell.length > 2){
          if (cell[2] == 90 || cell[2] == 270) cell.rail = "LRLR", cell.entrance = "LR";
        }
        break;
      case Board.SCISS:
        cell.rail = "DLDR";
        cell.entrance = "D";
        if (cell.length > 1){
            if (cell[1] == 90) cell.rail = "LDLU", cell.entrance = "L";
            else if (cell[1] == 180) cell.rail = "LURU", cell.entrance = "U";
            else if (cell[1] == 270) cell.rail = "DRRU", cell.entrance = "R";
        }
        break;
    }
  }
}
Board.prototype.update = function(justDraw){ // TODO: FIX!
    if (!(this.crashed || justDraw) || (this.TRACING && !this.crashedAll)){
        this.TIME++;
        //~ console.log(this.TIME);
        // Switch cell
        for (t = 0; t < this.trains.length; t++){
            tr = this.trains[t];
            if (tr.crashed) continue;
            if (tr[3] == 'U') tr[3] = 'D', tr[1]--;
            else if (tr[3] == 'D') tr[3] = 'U', tr[1]++;
            else if (tr[3] == 'L') tr[3] = 'R', tr[2]--;
            else if (tr[3] == 'R') tr[3] = 'L', tr[2]++;
        }
        // Move
        for (t = 0; t < this.trains.length; t++){
            tr = this.trains[t];
            if (tr.crashed) continue;
            cell = this.board[tr[1]][tr[2]];
            r = "----";
            if (typeof(cell)=="string") r = cell;
            else r = cell.rail;
            if (r.length == 2) r += r;
            // Do actual move
            if (r.indexOf(tr[3]) == -1 || cell instanceof Array && cell.entrance.indexOf(tr[3]) == -1){
                this.crash(tr)
                continue;
            }
            if (cell instanceof Array && cell[0] == Board.ARRIV){
                if (cell[1]==0 && tr[4] == Board.T_RED ||
                    cell[1]==1 && tr[4] == Board.T_BLUE){
                    this.trains.splice(t--, 1);
                    cell[1] = -1;
                    this.toFullfil--;
                } else {
                    this.crash(tr);
                }
                this.onCellRedraw(tr[1], tr[2]);
                continue;
            }
            if (cell instanceof Array && cell[0] == Board.PAINT){
                if (cell[1]==0)
                    tr[4] = Board.T_RED;
                else
                    tr[4] = Board.T_BLUE;
            }
            
            if (tr.shadow)
                delete tr.shadow;
            else if (cell instanceof Array && cell[0] == Board.SCISS){
                tmp = this.colorSplit(tr[4]);
                tr[4] = tmp[0];
                tr2 = [null, tr[1], tr[2], tr[3], tmp[1]];
                tr2.shadow = 1;
                this.trainPush(tr2);
            }
            
            if (r.substr(0,2).indexOf(tr[3]) != -1)
                tr[3] = r[1-r.substr(0,2).indexOf(tr[3])];
            else
                tr[3] = r[3-r.substr(2,2).indexOf(tr[3])];
            
            this.board[tr[1]][tr[2]] = this.cellUpdate(cell);
        }
        // Join
        for (t = 0; t < this.trains.length; t++){
            if (this.trains[t].crashed) continue;
            for (t2 = t+1; t2 < this.trains.length; t2++){
                if (this.trains[t2].crashed) continue;   
                if (this.trains[t][1] == this.trains[t2][1] &&
                    this.trains[t][2] == this.trains[t2][2] &&
                    this.trains[t][3] == this.trains[t2][3]){
                    this.trains[t][4] = this.colorJoin(this.trains[t][4], this.trains[t2][4]);
                    this.trains.splice(t2--, 1);
                }
            }
        }
        
        this.crashedAll = true;
        for (t = 0; t < this.trains.length; t++)
            if (!this.trains[t].crashed){
                this.crashedAll = false;
                this.onCellRedraw(this.trains[t][1], this.trains[t][2]);
                if (this.markers[this.trains[t][1]+","+this.trains[t][2]])
                    this.onMarkerHit(this.trains[t][1],this.trains[t][2],this.markers[this.trains[t][1]+","+this.trains[t][2]]);
            }
        
        if (this.crashed && !this.TRACING)
            this.onCrashed();
        if (this.toFullfil==0 && !this.TRACING)
            this.onDone();
            
    }
    // Redraw
    //~ for (r = 0; r < this.board.length; r++)
        //~ for (c = 0; c < this.board[r].length; c++)
            //~ if (typeof(this.board[r][c])=="string" || this.board[r][c].dynamic)
                //~ this._board[r][c].html("").append(this.drawCell(this.board[r][c]).attr("title",r+","+c));
    //~ console.log(this.trains);
    
    this.onBeforeTrainDraw();
    for (t = 0; t < this.trains.length; t++)
        if (this.TRACING){
            //~ if (this.fixingPlayground) continue;
            this.onTrainTrace(this.trains[t]);
        } else
            this.onTrainDraw(this.trains[t]);
}

Board.prototype.onBeforeTrainDraw = function(){ // If needed, for animation
    // Nothing by default
}
Board.prototype.onTrainDraw = function(tr){
    console.log("Unimplemented :(");
}
Board.prototype.onTrainTrace = function(tr){
    console.log("Unimplemented :(");
}
Board.prototype.onCellRedraw = function(r,c){
    console.log("Unimplemented :(");
}

Board.prototype.redraw = function(){
    this.toFullfil = 0;
    for (r = 0; r < this.board.length; r++)
        for (c = 0; c < this.board[r].length; c++){
            if (this.board[r][c][0] == Board.ARRIV && this.board[r][c][1] != -1) this.toFullfil++;
            this.cellSetup(r,c);
            this.onCellRedraw(r,c);
        }
}
Board.prototype.crash = function(tr){
    this.crashed = true;
    tr.crashed = true;
    if (this.TRACING) return;
}
Board.prototype.colorJoin = function(c1, c2){
    if (c1 != c2)
        c = c1 + c2;
    else
        c = c1;
    if (!Board.T_C[c]) c = Board.T_BROWN;
    return c;
}
Board.prototype.colorSplit = function(c){
    if (c == Board.T_RED || c == Board.T_BLUE || c == Board.T_YELLOW || c == Board.T_BROWN)
        return [c,c];
    else if (c == Board.T_PURPLE)
        return [Board.T_BLUE, Board.T_RED];
    
}
Board.prototype.cellUpdate = function(cell){
    if (typeof(cell)=="string" && cell.length == 4)
        cell = cell.substr(2,2)+cell.substr(0,2);
    else if (cell instanceof Array && cell[0] == Board.SCISS)
        cell.rail = cell.rail.substr(2,2)+cell.rail.substr(0,2);
    return cell;
}
Board.prototype.clone = function(o){
    return JSON.parse(JSON.stringify(o));
}
Board.prototype.stateSave = function(){
    this.state.push([this.clone(this.board),this.clone(this.trains),this.clone(this.TIME),this.clone(this.crashed),this.clone(this.crashedAll),this.clone(this.markers)]);
}
Board.prototype.stateLoad = function(){
    s = this.state.pop();
    this.board = s[0];
    this.trains = s[1];
    this.TIME = s[2];
    this.crashed = s[3];
    this.crashedAll = s[4];
    this.markers = s[5];
    this.redraw();
}
Board.prototype.trainTrace = function(){
    this.stateSave();
    this.TRACING = true;
    while (!(this.crashedAll || this.abortTracing)){
        this.update();
        if (this.TIME > 1000) break;
    }
    this.abortTracing = false;
    this.TRACING = false;
    this.stateLoad();
}
Board.prototype.toggleOption = function(r,c){
    //~ this.toggledList.push([r,c]);
    if (this.board[r-1][c] == "DU"){
        this.board[r-1][c] = "DR"; this.onCellRedraw(r-1,c);
        this.board[r-2][c] = [Board.EMPTY]; this.onCellRedraw(r-2,c);
        this.board[r-1][c+1] = "LD"; this.onCellRedraw(r-1,c+1);
        this.board[r][c+1] = "URDR"; this.onCellRedraw(r,c+1);
    } else {
        this.board[r-1][c] = "DU"; this.onCellRedraw(r-1,c);
        this.board[r-2][c] = "DU"; this.onCellRedraw(r-2,c);
        this.board[r-1][c+1] = [Board.EMPTY]; this.onCellRedraw(r-1,c+1);
        this.board[r][c+1] = "DR"; this.onCellRedraw(r,c+1);
    }
}
Board.prototype.playgroundFix = function(){
    this.fixingPlayground = true;
    while (true){
        //~ this.arrivalList = [];
        this.trainTrace();
        if (!this.playgroundAugment()) break;
    }
    this.stateLoad(); // reset
    this.setup();
    this.fixingPlayground = false;
}
Board.prototype.playgroundAugment = function(){
    if (!this._tmpPath) return;
    this.stateLoad(); // reset
    r = this._tmpPath[0];
    mk = this._tmpPath[1];
    this.hittingDones[mk[0]] = true;
    delete this._tmpPath;
    a = this.hittingTime[mk[0]];
    c = this.hittingTime[mk[0]][2]-6-1;
    this.hittingTime = [];
    //~ console.log(a);
    if (a[0] < a[1]){
        r = r-1;
        c = c;
        i = 0; j = 1;
        ti = 0; tj = 3;
        z = Math.max(a[1]-a[0]-1+4,3);
        lastStep = "R";
    } else {
        r = r-1;
        c = c+5;
        i = 0; j = 2;
        ti = 0; tj = 0;
        z = Math.max(a[0]-a[1]-1+4,3);
        lastStep = "L";
    }
    b = this.getPath(4,deltaH,[i,j],[ti,tj],z);
    if (b == null){
    //~ console.log("Rect path:",4,deltaH);
    //~ console.log("Starting:",i,j);
    //~ console.log("Ending:",ti,tj);
    //~ console.log("ReqLength:",z);
        alert("Unable to sync paths in clause playground! Try to increase 'AND Buffer height' (in More Options)");
        return;
    }
    for (ii = 0; ii < b.length; ii++){
        //~ console.log(b[ii].join(" | "));
        for (jj = 0; jj < b[ii].length; jj++)
            this.board[r-ii][c+jj] = [0];
        }
    coming = "D";
    while (i != ti || j != tj){
        //~ console.log(i,j);
        if (i > 0 && b[i-1][j] == b[i][j]+1){
            this.board[r-i][c+j] = "D"+coming;
            coming = "U";
            i--;
        }
        if (j > 0 && b[i][j-1] == b[i][j]+1){
            this.board[r-i][c+j] = "L"+coming;
            coming = "R";
            j--;
        }
        if (i < b.length-1 && b[i+1][j] == b[i][j]+1){
            this.board[r-i][c+j] = "U"+coming;
            coming = "D";
            i++;
        }
        if (j < b[0].length-1 && b[i][j+1] == b[i][j]+1){
            this.board[r-i][c+j] = "R"+coming;
            coming = "L";
            j++;
        }
    }
    this.board[r-ti][c+tj] = lastStep+coming;
    this.setup();
    return true;
}
Board.prototype.onMarkerHit = function(r,c,mk){
    //~ console.log(r,c,mk);
    if (!this.fixingPlayground) return;
    //~ if (mk == -1) this.arrivalList.push([r,c]);
    if (this.hittingDones[mk[0]]) return;
    if (this.hittingTime[mk[0]]!==undefined && this.hittingTime[mk[0]][mk[1]]) return; // Already done
    if (this.hittingTime[mk[0]]===undefined) this.hittingTime[mk[0]] = [];
    this.hittingTime[mk[0]][mk[1]] = this.TIME;
    this.hittingTime[mk[0]][2] = Math.max(this.hittingTime[mk[0]][2]||0, c);
    if (this.hittingTime[mk[0]][1-mk[1]]){ // Got both
        this._tmpPath = [r,mk];
        this.abortTracing = true;
    }
}
Board.prototype.getPath = function(w,h,s,e,z){
  var b = [];
  foo = function(r,c){
    if (r == e[0] && c == e[1]){
      if (b[r][c] == z) return true;
      return false;
    }
    if (b[r][c] >= z) return false;
    if (r > 0 && b[r-1][c] == 0){
      b[r-1][c] = b[r][c]+1;
      if (foo(r-1, c)) return true;
      b[r-1][c] = 0;
    }
    if (c > 0 && b[r][c-1] == 0){
      b[r][c-1] = b[r][c]+1;
      if (foo(r, c-1)) return true;
      b[r][c-1] = 0;
    }
    if (r < h-1 && b[r+1][c] == 0){
      b[r+1][c] = b[r][c]+1;
      if (foo(r+1, c)) return true;
      b[r+1][c] = 0;
    }
    if (c < w-1 && b[r][c+1] == 0){
      b[r][c+1] = b[r][c]+1;
      if (foo(r, c+1)) return true;
      b[r][c+1] = 0;
    }
    return false;
  }
  for (var i = 0; i < h; i++){
    b[i] = [];
    for (var j = 0; j < w; j++)
      b[i][j] = 0;
  }
  b[s[0]][s[1]] = 1;
  if (!foo(s[0],s[1])) return null;
  return b;
}


var Model = {};
/* ForwardOnly
b = display([
            [1,1,"DU",1,1],
            [1,"DR","LURU","DL",1],
            [1,"RU",6,"LU",1],
            [1,1,"DU",1,1],
            [1,1,2,1,1]
        ], $("#gadget"));
b.pushTrain([null,4,2,"U",Board.T_PURPLE]);
*/
Model.OTP = [
                [1,1,"DU",1,1,1],
                [1,"DR","LU",1,1,1],
                [1,"RU",6,[4,0,90],1,1],
                [1,1,[5,0],1,1,1],
            ];
Model.OTPTurned = [
                [1,1,1,1,1],
                [1,1,"DR","LD",1,1],
                ["LR",[5,0,90],[6,90],"UR","LR"],
                [1,1,[4,0,180],1,1],
                [1,1,1,1,1],
            ];
Model.CannotCross = [
                [1,1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,"DR","LURU","DL",1,1,1,1],
                [1,1,1,1,1,"RU",6,"LU",1,1,1,1],
                [1,1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,1,"DU",1,"DR","LD",1,1],
                ["LR","LR","LR","LR","LR",[5,0,90],"LRDU","LR",[6,90],"UR","LR","LR"],
                [1,1,1,1,1,1,5,1,[4,0,180],1,1,1],
                [1,1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,1,"DU",1,1,1,1,1]
            ];
Model.CanCross = [
                [1,1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,"DR","RL","RL","RL","LUDU",1,1,1,1,1],
                [1,1,"DU",1,1,1,"DU",1,1,1,1,1],
                [1,"DR","RULU","DL",1,"DR","RULU","DL",1,1,1,1],
                [1,"RU",6,"LU",1,"RU",6,"LU",1,1,1,1],
                [1,1,"DU",1,1,1,"DU",1,1,1,1,1],
                [1,1,"DU",0,1,1,"DU",1,"DR","LD",1,1],
                ["LR","LR",[6,90,1],"DR","LR",[5,0,90],"LRDU","LR",[6,90],"UR","LR","LR"],
                [1,1,"UR","LU",1,1,5,1,[4,0,180],1,1,1],
                [1,1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,1,"DU",1,1,1,1,1]
            ];
Model.TunnelLong = [
                [1,1,1,1,1,1,1,"DU",1],
                [1,"DR","LR","LR","LR","LR","LR","LU",1],
                [1,"DU",1,1,1,1,1,1,1]
            ];
Model.TunnelShort = [
                [1,"DU",1],
                [1,"DU",1],
                [1,"DU",1]
            ];
Model.OneTimeAND = [ 
                [1,1,1,1,1,1,1,1,"DU",1,1],
                [1,1,1,1,1,"DR","LR","LR","LU",1,1],
                [1,1,1,1,1,"DU",1,1,1,1,1],
                [1,"DR","LR","LR","LR","LURU","LR","LR","LR","DL",1],
                [1,"DU",1,1,1,1,1,1,1,"DU",1],
                [1,"DU",1,1,[4,1,270],6,[4,0,90],1,1,"DU",1],
                [1,"DU",1,1,1,"DU",1,1,1,"DU",1],
                [1,"RU",6,"LR","LR","LURU","LR","LR",6,"LU",1],
                [1,1,"DU",1,1,1,1,1,"DU",1,1],
                [1,1,"UR","LR","LR",6,"LR","LR","LU",1,1]
            ];
Model.FreeWalk = [ 
                [1,1,1,1,1,1,1,1,"DU",1,1],
                [1,1,1,1,1,"DR","RL","RL","LU",1,1],
                [1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,"DU",1,1,1,1,1],
                [1,1,1,1,1,"DU",1,1,1,1,1]
            ];
Model.BottomPlayGround = [
                [0,"DR","LR","LR","LURU","LR","RL","LD",0]
            ];
Model.TunnelTop = [
                [1,1,1,1,1,1,1,1,1,1],
                [1,"DR","LR","LR","LR","LR","LR","LR","LD",1],
                [1,"DU",1,1,1,1,1,1,"DU",1]
            ];
Model.ReplicatorBegin = [
                [1,"DU",1],
                [1,[5,0],1],
                [[4,0,270],[6,180],"LR"],
                [1,1,1]
            ];
Model.ReplicatorUnit = [
                [1,1,1],
                ["DR","LD",1],
                [[6,90],"UR","LR"],
                ["UD",1,1]
            ];
Model.ReplicatorEnd = [
                [0,0],
                [1,1],
                [[5,0,90],[4,0,90]],
                [1,1]
            ];
//~ b.pushTrain([null,7,0,"R",Board.T_PURPLE]);
//~ b.pushTrain([null,13,6,"U",Board.T_BROWN]);

function Builder(F, K, K_I){
    getRow = function(type){
        var type = typeof type !== 'undefined' ? type : Board.EMPTY;
        var a = [];
        for (var i = 0; i < W; i++) a.push(type);
        return a;
    }
    copy = function(r, c, mod, overlay){
        for (a = 0; a < mod.length; a++)
            for (b = 0; b < mod[a].length; b++){
                board[r+a][c+b] = mod[a][b];
                //~ if (overlay) board[r+a][c+b].overlay = overlay;
            }
    }
    
    var V = [], varsStr = [];
    for (i = 0; i < F.length; i++){
        V = V.concat(F[i]);
        varsStr.push("("+F[i].join(" | ")+")");
    }
    varsStr = varsStr.join(" & ");
    
    V = V.filter(function (value, index, self) { return self.indexOf(value) === index; }).sort().reverse();
    //~ console.log(V);

    //~ deltaH = Math.min(6, V.length*F.length*2); // Playground Height
    deltaH = parseInt(Board.BUFFER_HEIGHT);
    deltaWperV = 4; // Width per var in green zone (> 3, must be even)
    deltaMatrix = deltaWperV*V.length+Model.OTPTurned[0].length;
    
    W = deltaMatrix+F.length*Model.CannotCross[0].length+Model.ReplicatorUnit[0].length*(V.length-K)+5;
    
    var board = [];
    var trains = [];
    
    // Spawner
    for (i = 0; i < 1+3*(V.length-K); i++) board.unshift(getRow());
    for (i = 0; i < V.length*deltaWperV; i++){
        if (i / deltaWperV < K)
            board[0][i] = (i % deltaWperV == 1 ? Board.SPAWN : Board.BLOCK);
        else
            board[0][i] = (i % deltaWperV == 1 ? "DU" : Board.BLOCK);
    }
    for (i = 0; i < 3*(V.length-K); i++)
        for (j = V.length*deltaWperV; j < deltaMatrix+Model.CannotCross[0].length*F.length; j++)
            board[1+i][j] = (i % 3 == 1 ? "LR" : Board.BLOCK);
    for (i = V.length*deltaWperV; i < W; i++) board[0][i] = Board.BLOCK;
    for (i = 0; i < V.length-K; i++){
        t = (V.length-i)*deltaWperV-4;
        for (j = t; j < V.length*deltaWperV; j++){
            board[1+i*3][j] = 1;
            board[1+i*3+1][j] = "LR";
            board[1+i*3+2][j] = 1;
        }
        for (j = 1+i*3+2; j > 0; j--){
            board[j][t] = 1;
            board[j][t+1] = "DU";
            board[j][t+2] = 1;
        }
        board[1+i*3+1][t+1] = "RU";
        board[1+i*3+1][t+2] = "LR";
        board[1+i*3+2][t+1] = Board.BLOCK;
        
        t = deltaMatrix+Model.CannotCross[0].length*F.length+4+i*3;
        for (j = deltaMatrix+Model.CannotCross[0].length*F.length; j < t+1; j++){
            board[1+i*3][j] = 1;
            board[1+i*3+1][j] = "LR";
            board[1+i*3+2][j] = 1;
        }
        for (j = 1+i*3+2; j >= 0; j--){
            board[j][t] = 1;
            board[j][t-1] = "DU";
            board[j][t-2] = 1;
        }
        board[1+i*3+1][t-1] = "UL";
        board[1+i*3+1][t-2] = "LR";
        board[1+i*3+2][t-1] = Board.BLOCK;
    }
    // Cross matrix
    for (i = 0; i < V.length; i++){
        lineY = 7;
        blockH = Model.CanCross.length;
        // Prepare rows
        for (j = 0; j < blockH; j++) board.unshift(getRow());
        /*
        // Extend wires
        for (j = 0; j < blockH; j++)
            for (k = 0; k < V.length-i; k++){
                if (k == V.length-i-1 && j <= lineY) continue;
                board[j][k*Model.OTP[0].length+1] = Board.BLOCK;
                board[j][k*Model.OTP[0].length+2] = "DU";
                board[j][k*Model.OTP[0].length+3] = Board.BLOCK;
            }
        board[lineY][(V.length-i-1)*Model.OTP[0].length+1] = Board.BLOCK;
        board[lineY][(V.length-i-1)*Model.OTP[0].length+2] = "DR";
        for (j = (V.length-i-1)*Model.OTP[0].length+1; j < Model.OTP[0].length*V.length; j++) board[lineY-1][j] = Board.BLOCK;
        for (j = (V.length-i-1)*Model.OTP[0].length+3; j < Model.OTP[0].length*V.length; j++) board[lineY][j] = "LR";
        for (j = (V.length-i-1)*Model.OTP[0].length+4; j < Model.OTP[0].length*V.length; j++) board[lineY+1][j] = Board.BLOCK;
        */
        //Setup crossing
        for (j = 0; j < F.length; j++){
            if (F[j].indexOf(V[i]) == -1){
                copy(0, deltaMatrix+j*Model.CannotCross[0].length, Model.CannotCross);
                board[0][deltaMatrix+j*Model.CannotCross[0].length] = {data: board[0][deltaMatrix+j*Model.CannotCross[0].length], rect: [Model.CannotCross[0].length, Model.CannotCross.length, Board.GH_CANNOTCROSS]};
            } else {
                copy(0, deltaMatrix+j*Model.CannotCross[0].length, Model.CanCross);
                board[lineY][deltaMatrix+j*Model.CannotCross[0].length+2] = {data:board[lineY][deltaMatrix+j*Model.CannotCross[0].length+2], toggler: i+","+j};
                board[0][deltaMatrix+j*Model.CannotCross[0].length] = {data: board[0][deltaMatrix+j*Model.CannotCross[0].length], rect: [Model.CanCross[0].length, Model.CanCross.length, Board.GH_CANCROSS]};
            }
        }
        board[lineY][deltaMatrix+F.length*Model.CannotCross.length] = {data: [Board.PAINT, 0, 90], rect: [2,1, Board.GH_TERMINAL]};
        board[lineY][deltaMatrix+F.length*Model.CannotCross.length+1] = [Board.ARRIV, 0, 90];
    }
    //GreenZone
    for (i = 0; i < deltaMatrix; i++) board[0][i] = Board.BLOCK;
    blockH = Model.CanCross.length;
    for (i = 0; i < V.length; i++){
        for (j = 0; j < blockH; j++) board[i*blockH+j][deltaWperV*V.length] = Board.BLOCK;
        copy(i*blockH+5, deltaWperV*V.length, Model.OTPTurned);
        board[i*blockH+5][deltaWperV*V.length] = {data: board[i*blockH+5][deltaWperV*V.length], rect: [Model.OTPTurned[0].length, Model.OTPTurned.length, Board.GH_OTP]};
    }
    for (i = 0; i < K; i++){
        for (j = K_I[i]*Model.CanCross.length+7; j < board.length-3*(V.length-K)-1; j++) board[j][i*deltaWperV+1] = "DU";
        for (j = i*deltaWperV+1; j < V.length*deltaWperV; j++) board[K_I[i]*Model.CanCross.length+7][j] = "LR";
        board[K_I[i]*Model.CanCross.length+7][i*deltaWperV+1] = "DR";
    }
    t = 0;
    for (i = K; i < V.length; i++){
        while (K_I.indexOf(t)!=-1) t++;
        for (j = t*Model.CanCross.length+7; j < board.length-3*(V.length-K)-1; j++) board[j][i*deltaWperV+1] = (board[j][i*deltaWperV+1] == 0 ? "DU" : board[j][i*deltaWperV+1]+"DU");
        for (j = i*deltaWperV+1; j < V.length*deltaWperV; j++) board[t*Model.CanCross.length+7][j] = "LR";
        board[t*Model.CanCross.length+7][i*deltaWperV+1] = "DR";
        t++;
    }
    board[1][0] = {data: board[1][0], rect: [deltaWperV*V.length, blockH*V.length-1, Board.GH_GREENZONE]};
    /*
    // Clauses OTP
    for (i = 0; i < 4; i++) board.unshift(getRow());
    for (i = 0; i < F.length; i++){
        copy(0, i*Model.CannotCross[0].length+deltaMatrix+4, Model.OTP);
        //~ board[0][i*Model.CannotCross[0].length+V.length*Model.OTP[0].length+4+2] = [Board.ARRIV, 0];
    }
    */
    
    // Clause playground
    for (i = 0; i < F.length-1; i++){
        for (j = 0; j < deltaH+2+3; j++) board.unshift(getRow());
        h = 0;
        for (j = i+1; j < F.length; j++){
            for (k = 0; k < 11; k++) board[0][i*Model.CannotCross[0].length+deltaMatrix+10 + k+h*12] = board[deltaH+1][i*Model.CannotCross[0].length+deltaMatrix+10 + k+h*12] = Board.BLOCK;
        }
        for (j = 0; j < deltaH; j++){
            board[j+1][i*Model.CannotCross[0].length+deltaMatrix+10+h*12] = board[j+1][i*Model.CannotCross[0].length+deltaMatrix+10+10+h*12] = Board.BLOCK;
            board[j+1][i*Model.CannotCross[0].length+deltaMatrix+10+h*12+5] = "DU";
        }
        copy(deltaH,i*Model.CannotCross[0].length+deltaMatrix+10+h*12+1,Model.BottomPlayGround);
            
        for (h = 0; h+i < F.length-1; h++){
            copy(deltaH+2,i*Model.CannotCross[0].length+deltaMatrix+10+h*12+7,Model.TunnelShort);
        }
        board[deltaH+1][i*Model.CannotCross[0].length+deltaMatrix+12] = {data:[Board.PAINT, 0], marker:[i,0]};
        board[deltaH+1][i*Model.CannotCross[0].length+deltaMatrix+12+6] = {data:[Board.PAINT, 1], marker:[i,1]};
        board[0][i*Model.CannotCross[0].length+deltaMatrix+12+3] = "DU";
        copy(deltaH+2,i*Model.CannotCross[0].length+deltaMatrix+5,Model.TunnelLong);
        
        //AND
        for (j = 0; j < Model.OneTimeAND.length; j++) board.unshift(getRow());
        
        for (h = 1; h+i < F.length-1; h++){
            //~ for (j = i+1; j < F.length; j++){
                //~ for (k = 0; k < 11; k++) 
                    //~ board[0][i*Model.CannotCross[0].length+V.length*Model.OTP[0].length+10 + k+h*12] = board[Model.OneTimeAND.length][i*Model.CannotCross[0].length+V.length*Model.OTP[0].length+10 + k+h*12] = Board.BLOCK;
            //~ }
            //~ for (j = 0; j < Model.OneTimeAND.length; j++)
                //~ board[j+1][i*Model.CannotCross[0].length+V.length*Model.OTP[0].length+10+h*12] = board[j+1][i*Model.CannotCross[0].length+V.length*Model.OTP[0].length+10+10+h*12] = Board.BLOCK;
            
            //~ board[0][i*Model.CannotCross[0].length+V.length*Model.OTP[0].length+10+8+h*12] = 
            //~ board[Model.OneTimeAND.length][i*Model.CannotCross[0].length+deltaMatrix+12+3+h*12] = 
            //~ board[Model.OneTimeAND.length+deltaH+1][i*Model.CannotCross[0].length+deltaMatrix+10+8+h*12] = "DU";
             
            //~ copy(0,i*Model.CannotCross[0].length+deltaMatrix+10+h*12,Model.FreeWalk);
            //~ copy(Model.OneTimeAND.length+1,i*Model.CannotCross[0].length+V.length*Model.OTP[0].length+10+h*12+1,Model.FakeConnectionSingle);
            for (j = 0; j < deltaH+Model.OneTimeAND.length+2; j++){
                board[j][i*Model.CannotCross[0].length+deltaMatrix+10+h*12+7] = board[j][i*Model.CannotCross[0].length+deltaMatrix+10+h*12+9] = Board.BLOCK;
                board[j][i*Model.CannotCross[0].length+deltaMatrix+10+h*12+8] = "DU";
            }
        }
        //~ copy(Model.OneTimeAND.length+1,i*Model.CannotCross[0].length+V.length*Model.OTP[0].length+10+1,Model.FakeConnection);
        copy(0,i*Model.CannotCross[0].length+deltaMatrix+10,Model.OneTimeAND);
        board[0][i*Model.CannotCross[0].length+deltaMatrix+10] = {data: board[0][i*Model.CannotCross[0].length+deltaMatrix+10], rect: [Model.OneTimeAND[0].length, Model.OneTimeAND.length+deltaH+2, Board.GH_AND]};
    }
    for (i = 0; i < Model.TunnelTop.length; i++) board.unshift(getRow());
    copy(0,(F.length-2)*Model.CannotCross[0].length+deltaMatrix+10+7,Model.TunnelTop);
    
    //Replicator
    copy(3, deltaMatrix+F.length*Model.CannotCross[0].length, Model.ReplicatorBegin);
    copy(3, deltaMatrix+F.length*Model.CannotCross[0].length+Model.ReplicatorBegin[0].length+(V.length - K)*Model.ReplicatorUnit[0].length, Model.ReplicatorEnd);
    for (i = 0; i < (V.length - K); i++){
        t = deltaMatrix+F.length*Model.CannotCross[0].length+Model.ReplicatorBegin[0].length+i*Model.ReplicatorUnit[0].length;
        copy(3, t, Model.ReplicatorUnit);
        for (j = 3+Model.ReplicatorUnit.length; j < board.length-3*(V.length-K)-1; j++){
            board[j][t-1] = board[j][t+1] = Board.BLOCK;
            board[j][t] = "DU";
        }
    }
    board[3][deltaMatrix+F.length*Model.CannotCross[0].length] = {data: board[3][deltaMatrix+F.length*Model.CannotCross[0].length], rect: [Model.ReplicatorBegin[0].length+(V.length - K)*Model.ReplicatorUnit[0].length+Model.ReplicatorEnd[0].length, Model.ReplicatorUnit.length, Board.GH_REPLICATOR]};
    
    //Trains
    for (i = 0; i < K; i++){
        trains.push([null,board.length-3*(V.length-K)-1,i*deltaWperV+1,"U",Board.T_RED]);
    }
    
    //~ for (a = 0; a < board.length; a++)
        //~ for (b = 0; b < board[a].length; b++)
            //~ console.log(board[a][b]);
    
    return [board, trains, V, F, varsStr, deltaH];
}
    
function init(f, k, k_i){
    f = f.split(" ");
    for (i = 0; i < f.length; i++) f[i] = f[i].split("|");
    bb = Builder(f, k, k_i);
    sw = {}; // Switcher (for canCross)
    mk = {}; // Markers (for fix playground)
    rs = []; // Rects (for gadget overlay) [x,y,w,h,col]
    //~ console.log(bb[0]);
    for (a = 0; a < bb[0].length; a++){
        //~ console.log(a);
        for (b = 0; b < bb[0][a].length; b++){
            if (bb[0][a][b].toggler){
                sw[bb[0][a][b].toggler] = a+","+b;
                bb[0][a][b] = bb[0][a][b].data;
            }
            if (bb[0][a][b].marker){
                mk[a+","+b] = bb[0][a][b].marker;
                bb[0][a][b] = bb[0][a][b].data;
            }
            if (bb[0][a][b].rect){
                bb[0][a][b].rect.unshift(a);
                bb[0][a][b].rect.unshift(b);
                rs.push(bb[0][a][b].rect);
                bb[0][a][b] = bb[0][a][b].data;
            }
            bb[0][a][b] = (bb[0][a][b] instanceof Array || typeof(bb[0][a][b])=="string" ? bb[0][a][b] : [bb[0][a][b]]);
        }
    }
    b = new Board(bb[0], bb[1], mk);
    //~ for (i = 0; i < bb[1].length; i++) b.trainPush(bb[1][i]);
    return [b, bb, sw, rs];
}

