'use strict';

// A block in a board
class Block {
    constructor(type, rail = "----", entrance = '', rotation = 0, color = 0){
        this.type = type;
        this.rail = rail;
        this.entrance = entrance;
        this.rotation = rotation;
        this.color = color;
        this.normalizeRail();
    }
    flipRails(){
        if (this.rail.length == 4)
            this.rail = this.rail.substr(2,2)+this.rail.substr(0,2);
    }
    normalizeRail(){
        var tmp = "";
        for (var i = 0; i < this.rail.length; i += 2){
            if (this.rail[i] > this.rail[i+1])
                tmp += this.rail[i+1]+this.rail[i];
            else
                tmp += this.rail[i]+this.rail[i+1];
        }
        this.rail = tmp;
    }
    clone(){ return new Block(clone(this.type), clone(this.rail), clone(this.entrance), clone(this.rotation), this.color); }
}
var BlockType = {
    Empty    : ' ', // empty cell
    Rock     : '#', // rock cell
    Spawner  : 'S', // departure station (spawner)
    Arrival  : 'T', // arrival station
    Painter  : 'P', // painter
    Splitter : 'X', // splitter (scissor)
    // Color    : '@', // color filled cell
    Rail     : '-',
}

class Color {
    constructor(name, id){
        this.name = name;
        this.id = id;
    }
    split(){
        if (this == Colors.Purple)
            return [Colors.Blue, Colors.Red];
        else
            return [this, this];
    }
    static join(c1, c2){
        return ColorsById[c1 != c2 ? c1.id + c2.id : c1.id] || Colors.Brown;
    }
}
var Colors = {
    Red: new Color("red", 1),
    Blue: new Color("blue", 2),
    Yellow: new Color("yellow", 4),
    Purple: new Color("purple", 1+2),
    Brown: new Color("brown", 100),
    Satisfied: new Color("satisied", 1000)
}
var ColorsById = {};
for (var color in Colors) if (Colors.hasOwnProperty(color)) ColorsById[Colors[color].id] = Colors[color];

class Train {
    constructor(r, c, cellPosition, color, crashed = false){
        this.r = r;
        this.c = c;
        this.cellPosition = cellPosition;
        this.color = color;
        this.crashed = crashed;
    }
    clone(){
        return new Train(this.r, this.c, this.cellPosition, this.color, this.crashed);
    }
}

class Board{
    // board configuration (Block[][]), train list (Train[] or true to get them from the board) and marker points ({cellString_i: {index, arg}})
    constructor(board, trains = true, markers = []){
        this.board = board;
        this.markers = markers;
        this.time = 0;
        this.crashed = this.crashedAll = false;
        this.stateStack = [];
        this.markerHitInfo = [];
        this.hittingDones = [];
        this.trains = [];
        if (trains === true) trains = this.trainSpawned();
        for (var i = 0; i < trains.length; i++) this.trainPush(trains[i]);
        this.setup();
    }
    // Virtual methods but that may be 
    cellDraw(r,c){ /*throw"Unimplemented :(";*/ }
    trainDraw(train, tracing){ /*throw "Unimplemented :(";*/ }
    
    get height(){ return this.board.length; }
    get width(){ return this.board[0].length; }

    // State save/load (push/pop on states stack)
    stateSave(){
        var board = [], trains = [];
        for (var i = 0; i < this.board.length; i++){
            board[i] = [];
            for (var j = 0; j < this.board[i].length; j++)
                board[i][j] = this.board[i][j].clone();
        }
        for (var i = 0; i < this.trains.length; i++) trains.push(this.trains[i].clone());
        this.stateStack.push({board: board, trains: trains, time: this.time, crashed: this.crashed, crashedAll: this.crashedAll, markers: clone(this.markers)});
    }
    stateLoad(){
        var state = this.stateStack.pop();
        this.board = state.board;
        this.trains = state.trains;
        this.time = state.time;
        this.crashed = state.crashed;
        this.crashedAll = state.crashedAll;
        this.markers = state.markers;
        this.draw();
    }

    // Setup the board
    setup(draw = true){
        this.stateSave();
        this.onSetup(draw);
        if (draw) this.draw();
    }
    // Reset board state from states stack
    restart(draw = true){
        this.stateLoad();
        this.setup(draw);
    }
    // Add train to the board active trains
    trainPush(train){ this.trains.push(train); }

    trainCrash(train){
        this.crashed = true;
        train.crashed = true;
    }


    trainSpawned(){
        var trains = []
        for (var r = 0; r < this.height; r++)
            for (var c = 0; c < this.width; c++){
                if (this.board[r][c].type != BlockType.Spawner) continue;
                trains.push(new Train(r, c, "U", this.board[r][c].color));
            }
        return trains;
    }
    draw(){
        this.arrivalsEmpty = 0;
        for (var r = 0; r < this.height; r++)
            for (var c = 0; c < this.width; c++){
                if (this.board[r][c].type == BlockType.Arrival && this.board[r][c].color != Colors.Satisfied) this.arrivalsEmpty++;
                this.cellDraw(r,c);
            }
    }
    doStep(){
        if (!this.crashed || (this.isTracing && !this.crashedAll)){
            this.time++;
            // Switch cell
            for (var t = 0; t < this.trains.length; t++){
                var train = this.trains[t];
                if (train.crashed) continue;
                if (train.cellPosition == 'U') train.cellPosition = 'D', train.r--;
                else if (train.cellPosition == 'D') train.cellPosition = 'U', train.r++;
                else if (train.cellPosition == 'L') train.cellPosition = 'R', train.c--;
                else if (train.cellPosition == 'R') train.cellPosition = 'L', train.c++;
                if (train.r < 0 || train.r > this.board.length || train.c < 0 || train.c > this.board[0].length)
                    train.crashed = true;
            }
            // Move
            for (var t = 0; t < this.trains.length; t++){
                var train = this.trains[t];
                if (train.crashed) continue;
                var cell = this.board[train.r][train.c];
                // Do actual move
                if (cell.rail.indexOf(train.cellPosition) == -1 || cell.type != BlockType.Rail && cell.entrance.indexOf(train.cellPosition) == -1){
                    this.trainCrash(train)
                    continue;
                }
                if (cell.type == BlockType.Arrival){
                    if (cell.color == train.color){
                        this.trains.splice(t--, 1);
                        cell.color = Colors.Satisfied;
                        this.arrivalsEmpty--;
                    } else {
                        this.trainCrash(train);
                    }
                    this.cellDraw(train.r, train.c);
                    continue;
                } else if (cell.type == BlockType.Painter)
                    train.color = cell.color;
                
                if (train.skipProcess)
                    delete train.skipProcess;
                else if (cell.type == BlockType.Splitter){
                    var newColors = train.color.split();
                    train.color = newColors[0];
                    var newTrain = train.clone();
                    newTrain.color = newColors[1];
                    newTrain.skipProcess = 1;
                    this.trainPush(newTrain);
                }
                
                var flippingTrain = cell.rail.substr(0,2).indexOf(train.cellPosition);
                if ( flippingTrain != -1)
                    train.cellPosition = cell.rail[1-flippingTrain];
                else
                    train.cellPosition = cell.rail[3-cell.rail.substr(2,2).indexOf(train.cellPosition)];
                
                this.board[train.r][train.c].flipRails();
            }
            // Join
            for (var t1 = 0; t1 < this.trains.length; t1++){
                if (this.trains[t1].crashed) continue;
                for (var t2 = t1+1; t2 < this.trains.length; t2++){
                    if (this.trains[t2].crashed) continue;   
                    if (this.trains[t1].r == this.trains[t2].r &&
                        this.trains[t1].c == this.trains[t2].c){
                        if (this.trains[t1].cellPosition == this.trains[t2].cellPosition){ // Train merge
                            this.trains[t1].color = Color.join(this.trains[t1].color, this.trains[t2].color);
                            this.trains.splice(t2--, 1);
                        } else if (cell.rail.length == 2 || cell.rail == "LRDU" || cell.rail == "DULR"){ // Train touch
                            this.onTrainTouch(this.trains[t1], this.trains[t2]);
                        }
                    }
                }
            }
            
            this.crashedAll = true;
            for (var t = 0; t < this.trains.length; t++)
                if (!this.trains[t].crashed){
                    this.crashedAll = false;
                    this.cellDraw(this.trains[t].r, this.trains[t].c);
                    if (this.markers[this.trains[t].r+","+this.trains[t].c])
                        this.onMarkerHit(this.trains[t].r, this.trains[t].c, this.markers[this.trains[t].r+","+this.trains[t].c]);
                }
            
            if (!this.isTracing){
                if (this.crashed)
                    this.onCrash();
                if (this.arrivalsEmpty == 0 && this.trains.length == 0)
                    this.onSuccess();
                else if (this.arrivalsEmpty != 0 && this.trains.length == 0)
                    this.onFail();
            }   
        }
        this.onBeforeTrainsDraw();
        for (var t = 0; t < this.trains.length; t++)
            this.trainDraw(this.trains[t], this.isTracing);
    }

    trainTrace(){
        this.stateSave();
        this.isTracing = true;
        while (!(this.crashedAll || this.abortTracing)){
            this.doStep();
            if (this.time > 10000) break;
        }
        this.abortTracing = false;
        this.isTracing = false;
        this.stateLoad();
    }

    // Callbacks
    onSetup(draw){}
    onBeforeTrainsDraw(){}
    onCrash(){}
    onFail(){}
    onSuccess(){}
    onMarkerHit(r,c,marker){}
    onTrainTouch(t1,t2){}
}

/* == Util funcs == */
// Deep clone a json-ifiable obj
function clone(obj){
    var cloned = JSON.parse(JSON.stringify(obj));
    cloned.__proto__ = obj.__proto__;
    return cloned;
}

/*
Convert a charBoard into a Board
Params:
    board: matrix representing the board, with BlockType.*, [BlockType.*, params...] or {data: [BlockType.*, params...], options.. } as entries
*/
function charToBoard(charBoard){
    var board = clone(charBoard);
    var togglers = {}; // Togglers for canCross
    var markers = {}; // Markers (for fix playground)
    var highlightRects = []; // Rects for gadget highlights [x,y,w,h,col]
    for (var r = 0; r < board.length; r++)
        for (var c = 0, cell; c < board[r].length; c++){
            var cellString = r+","+c;
            if (board[r][c].toggler){
                togglers[board[r][c].toggler] = cellString;
            }
            if (board[r][c].marker){
                markers[cellString] = board[r][c].marker;
            }
            if (board[r][c].rect){
                highlightRects.push(Object.assign({r: r, c: c}, board[r][c].rect));
            }
            if (board[r][c].data)
                cell = board[r][c].data;
            else
                cell = board[r][c];
            if (cell == '') cell = ' ';
            if (typeof(cell)=="string" && cell.length == 1)
                cell = [cell];
            var block;
            if (cell instanceof Array){
                block = new Block(cell[0])
                switch (block.type) {
                    case BlockType.Arrival:
                        block.rail = "DUDU";
                        block.entrance = "D";
                        if (cell.length > 1){
                            block.color = (cell[1] == 0 ? Colors.Red : Colors.Blue);
                        }
                        if (cell.length > 2){
                            block.rotation = cell[2];
                            if (block.rotation == 90 || block.rotation == 270) block.rail = "LRLR";
                            if (block.rotation == 90) block.entrance = "L";
                            if (block.rotation == 180) block.entrance = "U";
                            if (block.rotation == 270) block.entrance = "R";
                        }
                        break;
                    case BlockType.Painter:
                        block.rail = "DUDU";
                        block.entrance = "DU";
                        if (cell.length > 1){
                            block.color = (cell[1] == 0 ? Colors.Red : Colors.Blue);
                        } else
                            block.color = Colors.Blue;
                        if (cell.length > 2){
                            block.rotation = cell[2];
                            if (block.rotation == 90 || block.rotation == 270) block.rail = "LRLR", block.entrance = "LR";
                        }
                        break;
                    case BlockType.Splitter:
                        block.rail = "DLDR";
                        block.entrance = "D";
                        if (cell.length > 1){
                            block.rotation = cell[1];
                            if (block.rotation == 90) block.rail = "LDLU", block.entrance = "L";
                            else if (block.rotation == 180) block.rail = "LURU", block.entrance = "U";
                            else if (block.rotation == 270) block.rail = "DRRU", block.entrance = "R";
                        }
                        break;
                    case BlockType.Spawner:
                        if (cell.length > 1){
                            block.color = (cell[1] == 0 ? Colors.Red : Colors.Blue);
                        } else
                            block.color = Colors.Red;
                        break;
                    }
            } else {
                block = new Block(BlockType.Rail, cell);
            }
            block.normalizeRail();
            board[r][c] = block;
        }
    return {board: board, togglers: togglers, markers: markers, highlightRects: highlightRects};
}

// Get a path from src to dst in a w x h rect in exactly dist steps
function getPath(w, h, src, dst, dist){
    var matrix = [];
    var moves = [[-1,0],[0,-1],[1,0],[0,1]];
    var dfs = function(r,c){
        if (r == dst[0] && c == dst[1])
            return matrix[r][c] == dist;
        if (matrix[r][c] >= dist) return false;
        for (var i = 0; i < moves.length; i++){
            var r2 = r+moves[i][0], c2 = c+moves[i][1];
            if (Math.min(r2, c2) >= 0 && r2 < h && c2 < w && matrix[r2][c2] == 0){
                matrix[r2][c2] = matrix[r][c]+1;
                if (dfs(r2, c2)) return true;
                matrix[r2][c2] = 0;
            }
        }
        return false;
    }
    for (var i = 0; i < h; i++){
        matrix[i] = [];
        for (var j = 0; j < w; j++) matrix[i][j] = 0;
    }
    matrix[src[0]][src[1]] = 1;
    if (!dfs(src[0], src[1])) return null;
    return matrix;
}

// Returns a row of the specified blockChar type
function builderGetRow(width, blockChar){
    var blockChar = (typeof blockChar !== 'undefined' ? blockChar : BlockType.Empty);
    var row = [];
    for (var i = 0; i < width; i++) row.push(blockChar);
    return row;
}
// Copy a model into a board
function builderCopy(board, r, c, model){
    for (var a = 0; a < model.length; a++)
        for (var b = 0; b < model[a].length; b++){
            board[r+a][c+b] = model[a][b];
        }
}