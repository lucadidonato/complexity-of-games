class FakePST
{
    constructor()
    {
        this.points = [];
    }
    
    add_point(x, y)
    {
        this.points.push( {x: x, y: y} );
    }
    
    delete_point(y)
    {
        for(var i=0; i<this.points.length; i++)
        {
            if(this.points[i].y == y)
            {
                this.points.splice(i, 1);
                return;
            }
        }
    }
    
    min_y_in_x_range(x0)
    {
        var miny = null;
        for(var i=0; i<this.points.length; i++)
            if(this.points[i].x <= x0 && (miny==null || this.points[i].y < miny))
                miny = this.points[i].y;
        
        return miny;
    }
    
    get_x(y)
    {
        for(var i=0; i<this.points.length; i++)
            if(this.points[i].y==y)
                return this.points[i].x;
        
        return null;
    }
}

class ReduceFastestOracle
{
    constructor(bamboos, x) 
    {
        this.bamboos = [...bamboos];
        this.bamboos.sort((x, y) => { return y.rate - x.rate; });
        
        this.n = this.bamboos.length;
        this.day = 0;
        this.x = x;
        this.H = bamboos.reduce((total, current) => total + current.rate, 0);
        
        this.odd = true; //For drawing PST labels only
        this.pst1 = new FakePST();
        this.pst2 = new FakePST();
                
        for(var i=0; i<this.bamboos.length; i++)
            this.pst1.add_point(Math.ceil(this.H * this.x/this.bamboos[i].rate) - 1, i);
    }
    
    update(pst, x, y)
    {
        pst.delete_point(y);
        pst.add_point(Math.max(x, 0), y);
    }
    
    query()
    {
        var i = this.pst1.min_y_in_x_range(this.day);
        if(i!=null)
        {
            var next_cut = this.day + Math.ceil(this.H * this.x/this.bamboos[i].rate);
            this.update(this.pst1, next_cut, i);
            this.update(this.pst2, next_cut-this.n, i);
        }
        
        var x = this.pst1.get_x(this.day);
        this.update(this.pst2, x-this.n, this.day);
        
        this.day = (this.day+1)%this.n;
        if(this.day==0)
        {
            var t = this.pst2;
            this.pst2 = this.pst1;
            this.pst1 = t;
            this.odd = !this.odd;
        }
        
        return this.bamboos[i];
    }
}


class ReduceFastestOracleDrawer
{
    constructor(oracle, element)
    {
        this.oracle = oracle;
        this.pst_width = 1 + Math.ceil(this.oracle.H*this.oracle.x/this.oracle.bamboos[this.oracle.n-1].rate)*4;
        this.pst_gap = 20;
        this.xmargin = 30;

        this.zero = 16;
        this.width = this.xmargin + 2*this.pst_width + this.pst_gap + 10;
        this.hmax = this.oracle.n * 20;
        this.height = this.zero + this.hmax + 30;

        var canvas = document.createElement('canvas');
        canvas.style.width  = this.width.toString() + "px";
        canvas.style.height = this.height.toString() + "px";
        var dpr = window.devicePixelRatio || 1;
        canvas.width = this.width * dpr;
        canvas.height = this.height * dpr;
        this.ctx = canvas.getContext("2d");
        this.ctx.scale(dpr, dpr);
        element.appendChild(canvas);
        
        this.ctx.transform(1, 0, 0, -1, this.xmargin, this.height - this.zero);

        this.ctx.imageSmoothingEnabled = false;
    }
    
    line(x0, y0, x1, y1, color, thickness)
    {
        this.ctx.strokeStyle = this.ctx.fillStyle  = color;
        this.ctx.lineWidth = thickness;
        this.ctx.beginPath();
        this.ctx.moveTo(x0, y0);
        this.ctx.lineTo(x1, y1);
        this.ctx.stroke();  
    }
    
    dot(x, y, color)
    {
         this.ctx.fillStyle = color;
         this.ctx.beginPath();
         this.ctx.arc(x, y, 4, 0, 2 * Math.PI);
         this.ctx.fill();
    }
    
    text(s, x, y, font, color, align, baseline)
    {
        this.ctx.font = font;
        this.ctx.strokeStyle = this.ctx.fillStyle  = color;
        this.ctx.textAlign = align;
        this.ctx.textBaseline = baseline;

        this.ctx.save();
        this.ctx.scale(1, -1);
        this.ctx.fillText(s, x, -y);
        this.ctx.restore();
    }
    
    clear()
    {
        this.ctx.clearRect(-this.xmargin, -this.zero, this.width, this.height);
    }
    
    draw()
    {
        this.clear();
        
        var pst1 = this.oracle.odd?this.oracle.pst1:this.oracle.pst2;
        var pst2 = this.oracle.odd?this.oracle.pst2:this.oracle.pst1;
        
        //Axes first pst
        this.line(0, 0, 0, this.hmax + 3, "#000000", 1.5);
        this.line(-3, 0, this.pst_width, 0, "#000000", 1.5);
                
        //Axes second pst
        this.line(this.pst_width + this.pst_gap, 0, this.pst_gap + this.pst_width, this.hmax + 3, "#000000", 1.5);
        this.line(this.pst_gap + this.pst_width -3, 0, this.pst_gap+ 2*this.pst_width, 0, "#000000", 1.5);

        for(var i=1; i<=this.oracle.n; i++)
        {            
            //Horizontal lines
            this.line(0, i*20, this.pst_width, i*20, "#BBBBBB", 1);
            this.line(this.pst_gap + this.pst_width, i*20, this.pst_gap + 2*this.pst_width, i*20, "#BBBBBB", 1);
            
            //Y tickers
            this.line(-3, i*20, 0, i*20, "#000000", 1);
            this.line(this.pst_gap + this.pst_width -3, i*20, this.pst_gap + this.pst_width, i*20, "#000000", 1);
            this.text("b"+(this.oracle.bamboos[i-1].tag+1), -5, i*20, "14px sans-serif", "#000000", "right", "middle")
        }
 
        //X tickers
        for(var i=0; i<=this.pst_width/4; i+=8)
        {
            this.line(i*4, 0, i*4, -3, "#000000", 1);
            this.line(this.pst_gap + this.pst_width + i*4, 0, this.pst_gap + this.pst_width + i*4, -3, "#000000", 1);
            this.text(i.toString(), i*4, -4, "12px sans-serif", "#000000", "center", "top")
            this.text(i.toString(), this.pst_gap + this.pst_width + i*4,-4, "12px sans-serif", "#000000", "center", "top")
        }
 

        //Vertical "day" line and vertical pst1 -> pst2 line
        var offset = this.oracle.odd?0:(this.pst_gap + this.pst_width);
        this.line(offset +  this.oracle.day*4, 0, offset +  this.oracle.day*4, this.hmax, "#AA0000", 2)
        this.line(offset +  this.oracle.n*4, 0, offset +  this.oracle.n*4, this.hmax, "#0000AA", 2)
 
 
        //Dots first pst
        for(var i=0; i<pst1.points.length; i++)
        {
            var p = pst1.points[i];
            //if(p.x*4>this.pst_width)
            //    continue;
                
            var color = (this.oracle.odd || p.y<this.oracle.day)?"#000000":"#666666";
            this.dot(p.x*4, (p.y+1)*20, color);
        }
        
        //Dots second pst
        for(var i=0; i<pst2.points.length; i++)
        {
            var p = pst2.points[i];
            //if(p.x*4>this.pst_width)
            //    continue;
                
            var color = (!this.oracle.odd || p.y<this.oracle.day)?"#000000":"#666666";
            this.dot(this.pst_width + this.pst_gap + p.x*4, (p.y+1)*20, color);
        }

        this.text("A", this.pst_width/2, this.hmax+5, "18px sans-serif", "#000000", "center", "bottom");
        this.text("B", this.pst_gap + this.pst_width + this.pst_width/2, this.hmax+5, "18px sans-serif", "#000000", "center", "bottom");

    }
}

